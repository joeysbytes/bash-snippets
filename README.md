# Bash Snippets Summary

Look in the folder of each snippet for specific documentation.

| Directory | Script | Description |
| --- | --- | --- |
| colors | colors.sh | Provides ANSI 16-color code constants, as well as common ANSI attribute constants. |
| log | log.sh | A simple, but full-featured logging framework, with console and/or file output, and optional color support. |
| yes_no | yes_no.sh | Prompt the user with a yes/no question. |
