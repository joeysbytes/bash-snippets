# shellcheck shell=bash
# shellcheck disable=SC2034

###########################################################################
# This library provides a simple, but flexible, logging framework
# for your bash scripts.  It can log to the console and/or a file.
# ANSI colors are also supported.
#
# Below is how a default log entry is formatted:
#
# 2022-01-16 21:48:30 debug This is a log entry.
# +-----------------+ +---+ +------------------+
# |    date-time    | |log| |       log        |
# |      stamp      | |lvl| |       text       |
# +-----------------+ +---+ +------------------+
#        separator---^     ^--- separator
#
###########################################################################


###########################################################################
# Logging Global Variables
###########################################################################

# Logging Properties
_LOG_LEVEL=2              # 0=trace, 1=debug, 2=info, 3=warning, 4=error, 5=fatal
_LOG_COLOR_IND=0          # 0=no color, 1=color support
_LOG_LEVEL_FIELD_IND=1    # 0=do not show log level, 1=show log level
_LOG_TO_CONSOLE_IND=1     # 0=no console output, 1=console output
_LOG_TIMESTAMP_FORMAT="%Y-%m-%d %H:%M:%S"  # set to empty string to not use
_LOG_FILE_NAME=""         # set to empty string to not use
_LOG_FIELD_SEPARATOR=" "  # this is output between each field, set to empty string to not use

# Logging Levels
_LOG_LEVEL_TRACE=0
_LOG_LEVEL_DEBUG=1
_LOG_LEVEL_INFO=2
_LOG_LEVEL_WARN=3
_LOG_LEVEL_ERROR=4
_LOG_LEVEL_FATAL=5

# Logging Level Field Text
_LOG_LEVEL_FIELD_TEXT_TRACE="trace"
_LOG_LEVEL_FIELD_TEXT_DEBUG="debug"
_LOG_LEVEL_FIELD_TEXT_INFO="info "
_LOG_LEVEL_FIELD_TEXT_WARN="WARN "
_LOG_LEVEL_FIELD_TEXT_ERROR="ERROR"
_LOG_LEVEL_FIELD_TEXT_FATAL="FATAL"

# Logging Colors
_LOG_TRACE_COLOR="\033[33;49m"      # yellow on default
_LOG_DEBUG_COLOR="\033[32;49m"      # green on default
_LOG_INFO_COLOR="\033[36;49m"       # cyan on default
_LOG_WARN_COLOR="\033[30;43m"       # black on yellow
_LOG_ERROR_COLOR="\033[93;41m"      # bright yellow on red
_LOG_FATAL_COLOR="\033[97;45m"      # bright white on magenta
_LOG_TEXT_COLOR="\033[39;49m"       # default on default
_LOG_SEPARATOR_COLOR="\033[39;49m"  # default on default
_LOG_DEFAULT_COLOR="\033[39;49m"    # default on default

# Logging Output
_LOG_OUTPUT=""
_LOG_OUTPUT_COLOR=""


###########################################################################
# Logging Properties
###########################################################################

# For console output only, enable color support
function enable_log_color() {
    _LOG_COLOR_IND=1
}

# For console output only, disable color support
function disable_log_color() {
    _LOG_COLOR_IND=0
}

# Enable showing the field level in the log output
function enable_log_level_field() {
    _LOG_LEVEL_FIELD_IND=1
}

# Disable showing the field level in the log output
function disable_log_level_field() {
    _LOG_LEVEL_FIELD_IND=0
}

# Enable logging to the console
function enable_log_to_console() {
    _LOG_TO_CONSOLE_IND=1
}

# Disable logging to the console
function disable_log_to_console() {
    _LOG_TO_CONSOLE_IND=0
}

# Set the logging level to trace
function set_log_level_trace() {
    _LOG_LEVEL="${_LOG_LEVEL_TRACE}"
}

# Set the logging level to debug
function set_log_level_debug() {
    _LOG_LEVEL="${_LOG_LEVEL_DEBUG}"
}

# Set the logging level to info
function set_log_level_info() {
    _LOG_LEVEL="${_LOG_LEVEL_INFO}"
}

# Set the logging level to warning
function set_log_level_warning() {
    _LOG_LEVEL="${_LOG_LEVEL_WARN}"
}

# Set the logging level to warning
function set_log_level_warn() {
    _LOG_LEVEL="${_LOG_LEVEL_WARN}"
}

# Set the logging level to error
function set_log_level_error() {
    _LOG_LEVEL="${_LOG_LEVEL_ERROR}"
}

# Set the logging level to fatal
function set_log_level_fatal() {
    _LOG_LEVEL="${_LOG_LEVEL_FATAL}"
}

# Set the date format string that will be passed to the 'date' command.
# Setting this value to an empty string will cause this field to not be output.
# Not passing in a date format will cause the default format to be set.
#
# Parameters:
#   1 - date format string (optional)
function set_log_timestamp_format() {
    if [ $# -eq 0 ]
    then
        _LOG_TIMESTAMP_FORMAT="%Y-%m-%d %H:%M:%S"
    else
        _LOG_TIMESTAMP_FORMAT="${1}"
    fi
}

# Set the name of the log file.
# Setting this value to an empty string will disable file output.
# Not passing in a file name will disable file output.
#
# Parameters:
#   1 - file name of log
function set_log_file() {
    if [ $# -eq 0 ]
    then
        _LOG_FILE_NAME=""
    else
        _LOG_FILE_NAME="${1}"
    fi
}

# Set the string that will be used between the 3 fields of the log text.
# Not passing in a file name will set a default value of a single space.
#
# Parameters:
#   1 - log field separator
function set_log_field_separator() {
    if [ $# -eq 0 ]
    then
        _LOG_FIELD_SEPARATOR=" "
    else
        _LOG_FIELD_SEPARATOR="${1}"
    fi
}


###########################################################################
# Logging
###########################################################################

# This is the main logic for handling log requests.  It should not be
# called directly, instead use the specific log methods defined below.
#
# Parameters:
#   1 - log level of request
#   2 - text to be logged
function _log() {
    # If not output is defined, skip output
    if [ "${_LOG_TO_CONSOLE_IND}" -ne 1 ] && [ "${_LOG_FILE_NAME}" == "" ]
    then
        return 0
    fi

    # get parameters
    local log_level="${1}"
    local log_text="${2}"

    # If the requested log is lower than our set log level, skip output
    if [ "${log_level}" -lt "${_LOG_LEVEL}" ]
    then
        return 0
    fi

    # Build the log line
    _build_log_text "${log_level}" "${log_text}"

    # Output to console, if enabled
    if [ "${_LOG_TO_CONSOLE_IND}" -eq 1 ]
    then
        if [ "${_LOG_COLOR_IND}" -eq 1 ]
        then
            if [ "${log_level}" -ge "${_LOG_LEVEL_WARN}" ]
            then
                echo -e "${_LOG_OUTPUT_COLOR}" >&2
            else
                echo -e "${_LOG_OUTPUT_COLOR}"
            fi
        elif [ "${log_level}" -ge "${_LOG_LEVEL_WARN}" ]
        then
            echo -e "${_LOG_OUTPUT}" >&2
        else
            echo -e "${_LOG_OUTPUT}"
        fi
    fi

    # Output to log file, if enabled
    if [ ! "${_LOG_FILE_NAME}" == "" ]
    then
        echo "${_LOG_OUTPUT}">>"${_LOG_FILE_NAME}"
    fi
}

function log_trace() {
    _log "${_LOG_LEVEL_TRACE}" "${1}"
}

function log_debug() {
    _log "${_LOG_LEVEL_DEBUG}" "${1}"
}

function log_info() {
    _log "${_LOG_LEVEL_INFO}" "${1}"
}

function log_warning() {
    _log "${_LOG_LEVEL_WARN}" "${1}"
}

function log_warn() {
    _log "${_LOG_LEVEL_WARN}" "${1}"
}

function log_error() {
    _log "${_LOG_LEVEL_ERROR}" "${1}"
}

function log_fatal() {
    _log "${_LOG_LEVEL_FATAL}" "${1}"
}


###########################################################################
# Build Log Text
###########################################################################

# This function builds the actual log text to be output.
#
# Parameters:
#   1 - log level of request
#   2 - text to be logged
function _build_log_text() {
    _LOG_OUTPUT=""
    _LOG_OUTPUT_COLOR=""

    local log_level="${1}"
    local log_text="${2}"
    local log_build_temp
    local log_color_on
    local log_level_field_text
    local log_separator_length=${#_LOG_FIELD_SEPARATOR}
    local log_text_length="${#log_text}"

    # Set color on
    case "${log_level}" in
        0)
            log_color_on="${_LOG_TRACE_COLOR}"
            log_level_field_text="${_LOG_LEVEL_FIELD_TEXT_TRACE}"
            ;;
        1)
            log_color_on="${_LOG_DEBUG_COLOR}"
            log_level_field_text="${_LOG_LEVEL_FIELD_TEXT_DEBUG}"
            ;;
        2)
            log_color_on="${_LOG_INFO_COLOR}"
            log_level_field_text="${_LOG_LEVEL_FIELD_TEXT_INFO}"
            ;;
        3)
            log_color_on="${_LOG_WARN_COLOR}"
            log_level_field_text="${_LOG_LEVEL_FIELD_TEXT_WARN}"
            ;;
        4)
            log_color_on="${_LOG_ERROR_COLOR}"
            log_level_field_text="${_LOG_LEVEL_FIELD_TEXT_ERROR}"
            ;;
        5)
            log_color_on="${_LOG_FATAL_COLOR}"
            log_level_field_text="${_LOG_LEVEL_FIELD_TEXT_FATAL}"
            ;;
        *)
            log_color_on="${_LOG_DEFAULT_COLOR}"
            log_level_field_text="?????"
            ;;
    esac

    # Timestamp
    if [ ! "${_LOG_TIMESTAMP_FORMAT}" == "" ]
    then
        log_build_temp=$(date +"${_LOG_TIMESTAMP_FORMAT}")
        _LOG_OUTPUT="${_LOG_OUTPUT}${log_build_temp}"
        _LOG_OUTPUT_COLOR="${_LOG_OUTPUT_COLOR}${log_color_on}${log_build_temp}"
        if [ "${log_separator_length}" -gt 0 ]
        then
            _LOG_OUTPUT="${_LOG_OUTPUT}${_LOG_FIELD_SEPARATOR}"
            _LOG_OUTPUT_COLOR="${_LOG_OUTPUT_COLOR}${_LOG_SEPARATOR_COLOR}${_LOG_FIELD_SEPARATOR}"
        fi
    fi

    # Log Level
    if [ "${_LOG_LEVEL_FIELD_IND}" -eq 1 ]
    then
        _LOG_OUTPUT="${_LOG_OUTPUT}${log_level_field_text}"
        _LOG_OUTPUT_COLOR="${_LOG_OUTPUT_COLOR}${log_color_on}${log_level_field_text}"
        if [ "${log_separator_length}" -gt 0 ]
        then
            _LOG_OUTPUT="${_LOG_OUTPUT}${_LOG_FIELD_SEPARATOR}"
            _LOG_OUTPUT_COLOR="${_LOG_OUTPUT_COLOR}${_LOG_SEPARATOR_COLOR}${_LOG_FIELD_SEPARATOR}"
        fi
    fi

    # Log Text
    if [ "${log_text_length}" -gt 0 ]
    then
        _LOG_OUTPUT="${_LOG_OUTPUT}${log_text}"
        _LOG_OUTPUT_COLOR="${_LOG_OUTPUT_COLOR}${_LOG_TEXT_COLOR}${log_text}"
    fi
    _LOG_OUTPUT_COLOR="${_LOG_OUTPUT_COLOR}${_LOG_DEFAULT_COLOR}"
}
