# log.sh

This bash library provides a simple, but flexible, logging framework.

## Quick Start

| Logging | Set Log Level | Log Entry Formatting |
|---|---|---|
| log_trace *(stdout)* | set_log_level_trace | set_log_timestamp_format |
| log_debug *(stdout)* | set_log_level_debug | set_log_field_separator |
| log_info *(stdout)* | set_log_level_info | enable_log_color, disable_log_color |
| log_warn *(stderr)* | set_log_level_warn | enable_log_level_field, disable_log_level_field |
| log_warning *(stderr)* | set_log_level_warning | enable_log_to_console, disable_log_to_console |
| log_error *(stderr)* | set_log_level_error | set_log_file |
| log_fatal *(stderr)* | set_log_level_fatal | |

This is how the default log entries are formatted:

```text
2022-01-16 21:48:30 debug This is a log entry.
+-----------------+ +---+ +------------------+
|    date-time    | |log| |       log        |
|      stamp      | |lvl| |       text       |
+-----------------+ +---+ +------------------+
       separator---^     ^--- separator
```

## Reference

### Log Level Settings

The logging levels are as follows, in order:

| Logging Level | Outputs To |
|---|---|
| trace | stdout |
| debug | stdout |
| info | stdout |
| warn (or warning) | stderr |
| error | stderr |
| fatal | stderr |

No log entries lower than the logger level will be output.  For example, the default logger level is info, so any log entries for the debug or trace level will not be written out.

| Function | Default | What Levels Will Log |
|---|---|---|
| set_log_level_trace | no | trace, debug, info, warn, error, fatal |
| set_log_level_debug | no | debug, info, warn, error, fatal |
| set_log_level_info | yes | info, warn, error, fatal |
| set_log_level_warn<br />set_log_level_warning | no | warn, error, fatal |
| set_log_level_error | no | error, fatal |
| set_log_level_fatal | no | fatal |

### Output Settings

These functions control the formatting of the log entry, and where they output to.

| Function | Parameters | Default | Description |
|---|---|---|---|
| enable_log_color | _n/a_ | no | Enables color logging to the console. |
| disable_log_color | _n/a_ | yes | Disables color logging to the console. |
| enable_log_level_field | _n/a_ | yes | Enables the display of the log level field in the log entry.<br />(trace, debug, info, WARN, ERROR, FATAL)|
| disable_log_level_field | _n/a_ | no | Disables the display of the log level field in the log entry.<br />(trace, debug, info, WARN, ERROR, FATAL) |
| enable_log_to_console | _n/a_ | yes | Enables log entries to be output to the console. |
| disable_log_to_console | _n/a_ | no | Disables log entries to be output to the console. |
| set_log_timestamp_format | timestamp format | "%Y-%m-%d %H:%M:%S" | This is the format string passed to the 'date +' command.<br />If this is set to an empty string, the timestamp will not be output on the log entries.<br />If no argument is passed to this function, it will be reset to the default value. |
| set_log_file | file name | "" _(empty string)_ | Sets the name of the log file to output log entries to.<br />You do not have to worry about ANSI color codes outputting to the log file.<br />If the log file name is set to an empty string, no output will be sent to a log file.<br />If no argument is passed to this function, it will be set to the default value. |
| set_log_field_separator | field separator | _single space_ | Sets the text to output between log entry fields.<br />The fields are: timestamp, log level, log text<br />If no argument is passed to this function, it will be set to the default value. |

### Logging

These functions create the log entries.

| Function | Parameter | Description |
|---|---|---|
| log_trace | Text to log | Log an entry at the trace level. |
| log_debug | Text to log | Log an entry at the debug level. |
| log_info | Text to log | Log an entry at the info level. |
| log_warn<br />log_warning | Text to log | Log an entry at the warning level. |
| log_error | Text to log | Log an entry at the error level. |
| log_fatal | Text to log | Log an entry at the fatal level. |

## Advanced Logger Usage / Fine-Tuning the Logger

While I hope you find how the logger works to be most useful, you can do some more
adjustments by manually changing the variables listed below.  There are no
functions defined to assist with these changes.

### Alter The Default Log Colors

| Color Variable | Default Color | What This Affects |
|---|---|---|
| _LOG_TRACE_COLOR | yellow on default | Timestamp, Log Level Text for trace log entries. |
| _LOG_DEBUG_COLOR | green on default | Timestamp, Log Level Text for debug log entries.|
| _LOG_INFO_COLOR | cyan on default | Timestamp, Log Level Text for info log entries.|
| _LOG_WARN_COLOR | black on yellow | Timestamp, Log Level Text for warn/warning log entries.|
| _LOG_ERROR_COLOR | bright yellow on red | Timestamp, Log Level Text for error log entries.|
| _LOG_FATAL_COLOR | bright white on magenta | Timestamp, Log Level Text for fatal log entries.|
| _LOG_TEXT_COLOR | default on default | Log Text |
| _LOG_SEPARATOR_COLOR | default on default | The separators between log entry fields. |
| _LOG_DEFAULT_COLOR | default on default | The very last code in a log entry line. |

### Alter The Log Level Field Text

| Field Text Variable | Default | Log Level |
|---|---|---|
| _LOG_LEVEL_FIELD_TEXT_TRACE | trace | trace |
| _LOG_LEVEL_FIELD_TEXT_DEBUG | debug | debug |
| _LOG_LEVEL_FIELD_TEXT_INFO | info _(plus single space)_ | info |
| _LOG_LEVEL_FIELD_TEXT_WARN | WARN _(plus single space)_ | warn / warning |
| _LOG_LEVEL_FIELD_TEXT_ERROR | ERROR | error |
| _LOG_LEVEL_FIELD_TEXT_FATAL | FATAL | fatal |

## Getting The Last Log Output Text

For every log entry that is generated, 2 different log entries are stored in variables
before they are sent to output. If you need them after the fact, you can find them
in these variables.

| Output Variable | Description |
|---|---|
| _LOG_OUTPUT | The log entry generated without any color codes. |
| _LOG_OUTPUT_COLOR | The log entry generated with all color codes embedded. |

## Demo

To see a demo of the logger in action, run log_demo.sh from the demo directory.
