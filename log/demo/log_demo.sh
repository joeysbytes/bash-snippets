#!/usr/bin/env bash
set -e

source ../log.sh

LOG_FILE="/tmp/log_demo.log"

function main() {
    clear
    setup_logger

    echo "These are log lines with color enabled."
    echo ""
    write_log_lines

    echo ""
    echo "These are log lines with color disabled."
    echo ""
    disable_log_color
    write_log_lines

    echo ""
    echo "You can find the log file here: ${LOG_FILE}"
    echo ""
}


function write_log_lines() {
    log_trace "This is the trace line being logged."
    sleep 0.5
    log_debug "This is the debug line being logged."
    sleep 0.5
    log_info "This is the info line being logged."
    sleep 0.5
    log_warn "This is the warn line being logged."
    sleep 0.5
    log_error "This is the error line being logged."
    sleep 0.5
    log_fatal "This is the fatal line being logged."
}


function setup_logger() {
    enable_log_color
    enable_log_level_field
    enable_log_to_console
    set_log_level_trace
    set_log_file "${LOG_FILE}"
}


main
