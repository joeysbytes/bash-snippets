#!/usr/bin/env python3

import subprocess
import unittest
import datetime
from pathlib import Path


###########################################################################
# Utility function to run log.sh
###########################################################################

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
VARIABLES_DICT = {}
DEFAULT_VARIABLE_VALUES = { "_LOG_LEVEL": "2",
                            "_LOG_COLOR_IND": "0",
                            "_LOG_LEVEL_FIELD_IND": "1",
                            "_LOG_TO_CONSOLE_IND": "1",
                            "_LOG_TIMESTAMP_FORMAT": DATETIME_FORMAT,
                            "_LOG_FILE_NAME": "",
                            "_LOG_FIELD_SEPARATOR": " ",
                            "_LOG_LEVEL_TRACE": "0",
                            "_LOG_LEVEL_DEBUG": "1",
                            "_LOG_LEVEL_INFO": "2",
                            "_LOG_LEVEL_WARN": "3",
                            "_LOG_LEVEL_ERROR": "4",
                            "_LOG_LEVEL_FATAL": "5",
                            "_LOG_LEVEL_FIELD_TEXT_TRACE": "trace",
                            "_LOG_LEVEL_FIELD_TEXT_DEBUG": "debug",
                            "_LOG_LEVEL_FIELD_TEXT_INFO": "info ",
                            "_LOG_LEVEL_FIELD_TEXT_WARN": "WARN ",
                            "_LOG_LEVEL_FIELD_TEXT_ERROR": "ERROR",
                            "_LOG_LEVEL_FIELD_TEXT_FATAL": "FATAL",
                            "_LOG_TRACE_COLOR": "\033[33;49m",
                            "_LOG_DEBUG_COLOR": "\033[32;49m",
                            "_LOG_INFO_COLOR": "\033[36;49m",
                            "_LOG_WARN_COLOR": "\033[30;43m",
                            "_LOG_ERROR_COLOR": "\033[93;41m",
                            "_LOG_FATAL_COLOR": "\033[97;45m",
                            "_LOG_TEXT_COLOR": "\033[39;49m",
                            "_LOG_SEPARATOR_COLOR": "\033[39;49m",
                            "_LOG_DEFAULT_COLOR": "\033[39;49m",
                            "_LOG_OUTPUT": "",
                            "_LOG_OUTPUT_COLOR": "" }


def run_logsh(command):
    # set up command
    cmd = [ "/usr/bin/env", "bash", "./log_test_runner.sh", command ]

    # run the command
    status = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                            check=False, shell=False)
    # build results dictionary
    results = {}
    results["rc"] = status.returncode
    results["args"] = status.args
    results["stdout"] = status.stdout.decode('UTF-8')
    results["stderr"] = status.stderr.decode('UTF-8')

    # return results
    return results


###########################################################################
# Unit Test log.sh
###########################################################################

class TestLogSh(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Load all default variables in to the dictionary to cut down reads
        cmd = ""
        for key in DEFAULT_VARIABLE_VALUES.keys():
            cmd += 'echo -e ' + key + '~"${' + key + '}";'
        results = run_logsh(cmd)
        result_lines = results["stdout"].split('\n')
        for result_ln in result_lines:
            if len(result_ln) > 0:
                result_ln_parts = result_ln.split('~')
                VARIABLES_DICT[result_ln_parts[0]] = result_ln_parts[1]


    #----------------------------------------------------------------------
    # Testing utility methods
    #----------------------------------------------------------------------

    def check_results(self,
                      results, expected_rc, expected_stdout, expected_stderr,
                      expected_rc_msg="Return code did not match expected value",
                      expected_stdout_msg="stdout did not match expected value",
                      expected_stderr_msg="stderr did not match expected value"):
        self.assertEqual(expected_rc, results["rc"], expected_rc_msg)
        if expected_stdout is not None:
            self.assertEqual(expected_stdout, results["stdout"], expected_stdout_msg)
        if expected_stderr is not None:
            self.assertEqual(expected_stderr, results["stderr"], expected_stderr_msg)


    #----------------------------------------------------------------------
    # Validate default values
    #----------------------------------------------------------------------

    def test_default_values(self):
        for variable, value in DEFAULT_VARIABLE_VALUES.items():
            with self.subTest(variable=variable, value=value):
                self.assertEqual(value, VARIABLES_DICT[variable], "Default value not correct")


    #----------------------------------------------------------------------
    # Logging Properties Tests
    #----------------------------------------------------------------------

    # Log Color Indicator
    def test_enable_log_color(self):
        expected_rc = 0
        expected_stdout = "1"
        expected_stderr = ""
        cmd = '_LOG_COLOR_IND=999;enable_log_color;echo -e -n "${_LOG_COLOR_IND}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_disable_log_color(self):
        expected_rc = 0
        expected_stdout = "0"
        expected_stderr = ""
        cmd = '_LOG_COLOR_IND=999;disable_log_color;echo -e -n "${_LOG_COLOR_IND}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    # Log Level Field Indicator
    def test_enable_log_level_field(self):
        expected_rc = 0
        expected_stdout = "1"
        expected_stderr = ""
        cmd = '_LOG_LEVEL_FIELD_IND=999;enable_log_level_field;echo -e -n "${_LOG_LEVEL_FIELD_IND}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_disable_log_level_field(self):
        expected_rc = 0
        expected_stdout = "0"
        expected_stderr = ""
        cmd = '_LOG_LEVEL_FIELD_IND=999;disable_log_level_field;echo -e -n "${_LOG_LEVEL_FIELD_IND}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    # Log to Console Indicator
    def test_enable_log_to_console(self):
        expected_rc = 0
        expected_stdout = "1"
        expected_stderr = ""
        cmd = '_LOG_TO_CONSOLE_IND=999;enable_log_to_console;echo -e -n "${_LOG_TO_CONSOLE_IND}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_disable_log_to_console(self):
        expected_rc = 0
        expected_stdout = "0"
        expected_stderr = ""
        cmd = '_LOG_TO_CONSOLE_IND=999;disable_log_to_console;echo -e -n "${_LOG_TO_CONSOLE_IND}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    # Set Log Levels
    def test_set_log_level_trace(self):
        expected_rc = 0
        expected_stdout = "0"
        expected_stderr = ""
        cmd = '_LOG_LEVEL=999;set_log_level_trace;echo -e -n "${_LOG_LEVEL}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_level_debug(self):
        expected_rc = 0
        expected_stdout = "1"
        expected_stderr = ""
        cmd = '_LOG_LEVEL=999;set_log_level_debug;echo -e -n "${_LOG_LEVEL}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_level_info(self):
        expected_rc = 0
        expected_stdout = "2"
        expected_stderr = ""
        cmd = '_LOG_LEVEL=999;set_log_level_info;echo -e -n "${_LOG_LEVEL}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_level_warning(self):
        expected_rc = 0
        expected_stdout = "3"
        expected_stderr = ""
        cmd = '_LOG_LEVEL=999;set_log_level_warning;echo -e -n "${_LOG_LEVEL}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_level_warn(self):
        expected_rc = 0
        expected_stdout = "3"
        expected_stderr = ""
        cmd = '_LOG_LEVEL=999;set_log_level_warn;echo -e -n "${_LOG_LEVEL}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_level_error(self):
        expected_rc = 0
        expected_stdout = "4"
        expected_stderr = ""
        cmd = '_LOG_LEVEL=999;set_log_level_error;echo -e -n "${_LOG_LEVEL}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_level_fatal(self):
        expected_rc = 0
        expected_stdout = "5"
        expected_stderr = ""
        cmd = '_LOG_LEVEL=999;set_log_level_fatal;echo -e -n "${_LOG_LEVEL}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    # Timestamp format
    def test_set_log_timestamp_format_default_value(self):
        expected_rc = 0
        expected_stdout = DATETIME_FORMAT
        expected_stderr = ""
        cmd = '_LOG_TIMESTAMP_FORMAT=999;set_log_timestamp_format;echo -e -n "${_LOG_TIMESTAMP_FORMAT}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_timestamp_format_new_value(self):
        expected_rc = 0
        expected_stdout = "%Y%m%d"
        expected_stderr = ""
        cmd = '_LOG_TIMESTAMP_FORMAT=999;set_log_timestamp_format "%Y%m%d";echo -e -n "${_LOG_TIMESTAMP_FORMAT}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    # Log file name
    def test_set_log_file_default_value(self):
        expected_rc = 0
        expected_stdout = ""
        expected_stderr = ""
        cmd = '_LOG_FILE_NAME=999;set_log_file;echo -e -n "${_LOG_FILE_NAME}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_file_new_value(self):
        expected_rc = 0
        expected_stdout = "/tmp/test_log.txt"
        expected_stderr = ""
        cmd = '_LOG_FILE_NAME=999;set_log_file "/tmp/test_log.txt";echo -e -n "${_LOG_FILE_NAME}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    # Log field separator
    def test_set_log_field_separator_default_value(self):
        expected_rc = 0
        expected_stdout = " "
        expected_stderr = ""
        cmd = '_LOG_FIELD_SEPARATOR=999;set_log_field_separator;echo -e -n "${_LOG_FIELD_SEPARATOR}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test_set_log_field_separator_new_value(self):
        expected_rc = 0
        expected_stdout = "<>"
        expected_stderr = ""
        cmd = '_LOG_FIELD_SEPARATOR=999;set_log_field_separator "<>";echo -e -n "${_LOG_FIELD_SEPARATOR}"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    #----------------------------------------------------------------------
    # _build_log_text: Logging Output Building Tests
    #----------------------------------------------------------------------

    # The non-color output is checked field by field.
    # The color output is checked as a whole line.
    def check_log_output(self, results, log_msg, level,
                         dt_format=DATETIME_FORMAT, log_sep=None, log_lvl=True):
        # setup
        results_lines = results["stdout"].split('\n')
        results_plain = results_lines[0]
        results_color = results_lines[1]
        expected_color_output = ""
        results_idx = 0
        log_field_text = VARIABLES_DICT[f"_LOG_LEVEL_FIELD_TEXT_{level}"]
        log_separator = None
        if log_sep is None:
            log_separator = VARIABLES_DICT["_LOG_FIELD_SEPARATOR"]
        else:
            log_separator = log_sep
        main_color = VARIABLES_DICT[f"_LOG_{level}_COLOR"]
        separator_color = VARIABLES_DICT["_LOG_SEPARATOR_COLOR"]
        text_color = VARIABLES_DICT["_LOG_TEXT_COLOR"]
        default_color = VARIABLES_DICT["_LOG_DEFAULT_COLOR"]

        # check date
        if dt_format != "":
            results_date = results_plain[results_idx:19]
            dt = datetime.datetime.strptime(results_date, dt_format)
            self.assertEqual(results_date, dt.strftime(dt_format), "Timestamp appears incorrect")
            expected_color_output += main_color + results_date
            results_idx += 19
            # check separator 1
            if len(log_separator) > 0:
                results_separator_1 = results_plain[results_idx:results_idx+len(log_separator)]
                self.assertEqual(log_separator, results_separator_1, "Log separator 1 did not match")
                expected_color_output += separator_color + log_separator
                results_idx += len(log_separator)

        # check log level text
        if log_lvl:
            results_field_text = results_plain[results_idx:results_idx+len(log_field_text)]
            self.assertEqual(log_field_text, results_field_text, "Log Level Field Text did not match")
            expected_color_output += main_color + log_field_text
            results_idx += len(log_field_text)
            # check separator 2
            if len(log_separator) > 0:
                results_separator_2 = results_plain[results_idx:results_idx+len(log_separator)]
                self.assertEqual(log_separator, results_separator_2, "Log separator 2 did not match")
                expected_color_output += separator_color + log_separator
                results_idx += len(log_separator)

        # check log message
        if log_msg != "":
            results_msg = results_plain[results_idx:]
            self.assertEqual(log_msg, results_msg, "Log message did not match")
            expected_color_output +=  text_color + log_msg
            results_idx += len(log_msg)
        expected_color_output += default_color

        # check length of log line
        self.assertEqual(len(results_plain), results_idx, "Length of log line is not correct.")

        # check expected color output
        self.assertEqual(expected_color_output, results_color, "Output color line is incorrect")


    def test__build_log_text_default_output(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        log_msg = "This is a test"
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            with self.subTest(level=level):
                log_level = VARIABLES_DICT[f"_LOG_LEVEL_{level}"]
                cmd = '_build_log_text ' + log_level + ' "' + log_msg + '";'
                cmd += 'echo -e "${_LOG_OUTPUT}";'
                cmd += 'echo -e -n "${_LOG_OUTPUT_COLOR}"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)
                self.check_log_output(results, log_msg, level)


    def test__build_log_text_different_separator(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        log_msg = "This is a different separator test"
        log_separator = "-->"
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            with self.subTest(level=level):
                log_level = VARIABLES_DICT[f"_LOG_LEVEL_{level}"]
                cmd = 'set_log_field_separator "' + log_separator + '";'
                cmd += '_build_log_text ' + log_level + ' "' + log_msg + '";'
                cmd += 'echo -e "${_LOG_OUTPUT}";'
                cmd += 'echo -e -n "${_LOG_OUTPUT_COLOR}"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)
                self.check_log_output(results, log_msg, level, log_sep=log_separator)


    def test__build_log_text_no_separator(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        log_msg = "This is a blank separator test"
        log_separator = ""
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            with self.subTest(level=level):
                log_level = VARIABLES_DICT[f"_LOG_LEVEL_{level}"]
                cmd = 'set_log_field_separator "' + log_separator + '";'
                cmd += '_build_log_text ' + log_level + ' "' + log_msg + '";'
                cmd += 'echo -e "${_LOG_OUTPUT}";'
                cmd += 'echo -e -n "${_LOG_OUTPUT_COLOR}"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)
                self.check_log_output(results, log_msg, level, log_sep=log_separator)


    def test__build_log_text_no_date(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        log_msg = "This is a no-date output test"
        date_format = ""
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            with self.subTest(level=level):
                log_level = VARIABLES_DICT[f"_LOG_LEVEL_{level}"]
                cmd = 'set_log_timestamp_format "' + date_format + '";'
                cmd += '_build_log_text ' + log_level + ' "' + log_msg + '";'
                cmd += 'echo -e "${_LOG_OUTPUT}";'
                cmd += 'echo -e -n "${_LOG_OUTPUT_COLOR}"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)
                self.check_log_output(results, log_msg, level, dt_format=date_format)


    def test__build_log_text_different_date(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        log_msg = "This is a different date output test"
        date_format = "%Y%m%d_%H%M%S____"  # keep this at length 19 for easy checking
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            with self.subTest(level=level):
                log_level = VARIABLES_DICT[f"_LOG_LEVEL_{level}"]
                cmd = 'set_log_timestamp_format "' + date_format + '";'
                cmd += '_build_log_text ' + log_level + ' "' + log_msg + '";'
                cmd += 'echo -e "${_LOG_OUTPUT}";'
                cmd += 'echo -e -n "${_LOG_OUTPUT_COLOR}"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)
                self.check_log_output(results, log_msg, level, dt_format=date_format)


    def test__build_log_text_no_log_level_text(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        log_msg = "This is a no log level output test"
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            with self.subTest(level=level):
                log_level = VARIABLES_DICT[f"_LOG_LEVEL_{level}"]
                cmd = 'disable_log_level_field;'
                cmd += '_build_log_text ' + log_level + ' "' + log_msg + '";'
                cmd += 'echo -e "${_LOG_OUTPUT}";'
                cmd += 'echo -e -n "${_LOG_OUTPUT_COLOR}"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)
                self.check_log_output(results, log_msg, level, log_lvl=False)


    def test__build_log_text_empty_log_msg(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        log_msg = ""
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            with self.subTest(level=level):
                log_level = VARIABLES_DICT[f"_LOG_LEVEL_{level}"]
                cmd = '_build_log_text ' + log_level + ' "' + log_msg + '";'
                cmd += 'echo -e "${_LOG_OUTPUT}";'
                cmd += 'echo -e -n "${_LOG_OUTPUT_COLOR}"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)
                self.check_log_output(results, log_msg, level)


    #----------------------------------------------------------------------
    # _log: Logging Logic Tests
    #----------------------------------------------------------------------

    def test__log_all_output_disabled(self):
        expected_rc = 0
        expected_stdout = ""
        expected_stderr = ""
        cmd = 'disable_log_to_console;'
        cmd += '_log 5 "Testing all output disabled"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test__log_all_levels_low(self):
        expected_rc = 0
        expected_stdout = ""
        expected_stderr = ""
        for logger_level in range(0, 6):
            for log_entry_level in range(logger_level - 1, -1, -1):
                with self.subTest(logger_level=logger_level, log_entry_level=log_entry_level):
                    cmd = '_LOG_LEVEL=' + str(logger_level) + ';'
                    cmd += '_log ' + str(log_entry_level) + ' "Testing all levels low"'
                    results = run_logsh(cmd)
                    self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test__log_all_levels_no_color(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = None
        log_msg = "Testing all levels no color"
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        for level in levels:
            logger_level = int(VARIABLES_DICT[f"_LOG_LEVEL_{level}"])
            for log_entry_level in range(logger_level, 6):
                with self.subTest(level=level, logger_level=logger_level, log_entry_level=log_entry_level):
                    cmd = '_LOG_LEVEL=' + str(logger_level) + ';'
                    cmd += 'set_log_timestamp_format "";'
                    cmd += '_log ' + str(log_entry_level) + ' "' + log_msg + '"'
                    results = run_logsh(cmd)
                    log_level_field_text = VARIABLES_DICT[f"_LOG_LEVEL_FIELD_TEXT_{levels[log_entry_level]}"]
                    expected_text = f"{log_level_field_text} {log_msg}\n"
                    if log_entry_level < 3:
                        expected_stdout = expected_text
                        expected_stderr = ""
                    else:
                        expected_stdout = ""
                        expected_stderr = expected_text
                    self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test__log_all_levels_with_color(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = None
        log_msg = "Testing all levels with color"
        levels = [ "TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL" ]
        separator = VARIABLES_DICT["_LOG_FIELD_SEPARATOR"]
        separator_color = VARIABLES_DICT["_LOG_SEPARATOR_COLOR"]
        text_color = VARIABLES_DICT["_LOG_TEXT_COLOR"]
        default_color = VARIABLES_DICT["_LOG_DEFAULT_COLOR"]
        for level in levels:
            logger_level = int(VARIABLES_DICT[f"_LOG_LEVEL_{level}"])
            for log_entry_level in range(logger_level, 6):
                with self.subTest(level=level, logger_level=logger_level, log_entry_level=log_entry_level):
                    level_color = VARIABLES_DICT[f"_LOG_{levels[log_entry_level]}_COLOR"]
                    cmd = 'enable_log_color;'
                    cmd += '_LOG_LEVEL=' + str(logger_level) + ';'
                    cmd += 'set_log_timestamp_format "";'
                    cmd += '_log ' + str(log_entry_level) + ' "' + log_msg + '"'
                    results = run_logsh(cmd)
                    log_level_field_text = VARIABLES_DICT[f"_LOG_LEVEL_FIELD_TEXT_{levels[log_entry_level]}"]
                    expected_text = f"{level_color}{log_level_field_text}{separator_color}{separator}{text_color}{log_msg}{default_color}\n"
                    if log_entry_level < 3:
                        expected_stdout = expected_text
                        expected_stderr = ""
                    else:
                        expected_stdout = ""
                        expected_stderr = expected_text
                    self.check_results(results, expected_rc, expected_stdout, expected_stderr)


    def test__log_file_output(self):
        expected_rc = 0
        expected_stdout = None
        expected_stderr = ""
        dt = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        log_file = f"/tmp/log_test_{dt}.log"
        log_msg = "This is a test of log file writing."
        cmd = 'set_log_file "' + log_file + '";'
        cmd += '_log 2 "' + log_msg + '"'
        results = run_logsh(cmd)
        self.check_results(results, expected_rc, expected_stdout, expected_stderr)
        log_path = Path(log_file)
        self.assertTrue(log_path.is_file(), "Log file not found")
        log_file_text = log_path.read_text()
        log_path.unlink()
        self.assertEqual(results["stdout"], log_file_text, "Log file text did not match console output.")


    #----------------------------------------------------------------------
    # loggers: Test the individual log functions
    #----------------------------------------------------------------------

    def test_log_functions(self):
        test_params_list = [ ("TRACE", "trace"),
                             ("DEBUG", "debug"),
                             ("INFO", "info"),
                             ("WARN", "warn"),
                             ("WARN", "warning"),
                             ("ERROR", "error"),
                             ("FATAL", "fatal") ]
        for test_params in test_params_list:
            log_level = test_params[0]
            log_function = test_params[1]
            with self.subTest(log_level=log_level, log_function=log_function):
                log_level_field_text = VARIABLES_DICT[f"_LOG_LEVEL_FIELD_TEXT_{log_level}"]
                log_msg = f"Testing log_{log_function}"
                logging_level = int(VARIABLES_DICT[f"_LOG_LEVEL_{log_level}"])
                expected_text = f"{log_level_field_text} {log_msg}\n"
                expected_rc = 0
                expected_stdout = ""
                expected_stderr = ""
                if logging_level < 3:
                    expected_stdout = expected_text
                else:
                    expected_stderr = expected_text
                cmd = 'set_log_level_' + log_function + ';'
                cmd += 'set_log_timestamp_format "";'
                cmd += 'log_' + log_function + ' "' + log_msg + '"'
                results = run_logsh(cmd)
                self.check_results(results, expected_rc, expected_stdout, expected_stderr)


###########################################################################
# Begin
###########################################################################

if __name__ == "__main__":
    unittest.main()
