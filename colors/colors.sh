# shellcheck shell=bash
# shellcheck disable=SC2034

###########################################################################
# These are ANSI color constants, and some attributes, for use in bash
# scripts.
# Remember to use echo -e for output with these escape sequences.
###########################################################################


###########################################################################
# ANSI System Codes
###########################################################################
_ESC="\033"
_CSI="${_ESC}["
RESET="${_CSI}0m"
# clear screen, move cursor to 1,1
CLEAR_SCREEN="${_CSI}2J${_CSI}1;1H"


###########################################################################
# Foreground Colors
###########################################################################
DEFAULT="${_CSI}39m"
BLACK="${_CSI}30m"
RED="${_CSI}31m"
GREEN="${_CSI}32m"
YELLOW="${_CSI}33m"
BLUE="${_CSI}34m"
MAGENTA="${_CSI}35m"
CYAN="${_CSI}36m"
WHITE="${_CSI}37m"
BRIGHT_BLACK="${_CSI}90m"
BRIGHT_RED="${_CSI}91m"
BRIGHT_GREEN="${_CSI}92m"
BRIGHT_YELLOW="${_CSI}93m"
BRIGHT_BLUE="${_CSI}94m"
BRIGHT_MAGENTA="${_CSI}95m"
BRIGHT_CYAN="${_CSI}96m"
BRIGHT_WHITE="${_CSI}97m"


###########################################################################
# Alternate Foreground Color Constants
###########################################################################
DEFAULT_FG="${DEFAULT}"
BLACK_FG="${BLACK}"
RED_FG="${RED}"
GREEN_FG="${GREEN}"
YELLOW_FG="${YELLOW}"
BLUE_FG="${BLUE}"
MAGENTA_FG="${MAGENTA}"
CYAN_FG="${CYAN}"
WHITE_FG="${WHITE}"
BRIGHT_BLACK_FG="${BRIGHT_BLACK}"
BRIGHT_RED_FG="${BRIGHT_RED}"
BRIGHT_GREEN_FG="${BRIGHT_GREEN}"
BRIGHT_YELLOW_FG="${BRIGHT_YELLOW}"
BRIGHT_BLUE_FG="${BRIGHT_BLUE}"
BRIGHT_MAGENTA_FG="${BRIGHT_MAGENTA}"
BRIGHT_CYAN_FG="${BRIGHT_CYAN}"
BRIGHT_WHITE_FG="${BRIGHT_WHITE}"


###########################################################################
# Background Colors
###########################################################################
DEFAULT_BG="${_CSI}49m"
BLACK_BG="${_CSI}40m"
RED_BG="${_CSI}41m"
GREEN_BG="${_CSI}42m"
YELLOW_BG="${_CSI}43m"
BLUE_BG="${_CSI}44m"
MAGENTA_BG="${_CSI}45m"
CYAN_BG="${_CSI}46m"
WHITE_BG="${_CSI}47m"
BRIGHT_BLACK_BG="${_CSI}100m"
BRIGHT_RED_BG="${_CSI}101m"
BRIGHT_GREEN_BG="${_CSI}102m"
BRIGHT_YELLOW_BG="${_CSI}103m"
BRIGHT_BLUE_BG="${_CSI}104m"
BRIGHT_MAGENTA_BG="${_CSI}105m"
BRIGHT_CYAN_BG="${_CSI}106m"
BRIGHT_WHITE_BG="${_CSI}107m"


###########################################################################
# Misc Color Constants
###########################################################################
DEFAULT_COLORS="${DEFAULT_FG}${DEFAULT_BG}"


###########################################################################
# Attributes
###########################################################################
NORMAL="${_CSI}22m"
BOLD="${NORMAL}${_CSI}1m"
DIM="${NORMAL}${_CSI}2m"

ITALIC="${_CSI}3m"
ITALIC_ON="${ITALIC}"
ITALIC_OFF="${_CSI}23m"

UNDERLINE="${_CSI}4m"
UNDERLINE_ON="${UNDERLINE}"
UNDERLINE_OFF="${_CSI}24m"

BLINK="${_CSI}5m"
BLINK_ON="${BLINK}"
BLINK_OFF="${_CSI}25m"

REVERSE="${_CSI}7m"
REVERSE_ON="${REVERSE}"
REVERSE_OFF="${_CSI}27m"

STRIKETHROUGH="${_CSI}9m"
STRIKETHROUGH_ON="${STRIKETHROUGH}"
STRIKETHROUGH_OFF="${_CSI}29m"

HIDE="${_CSI}8m"
SHOW="${_CSI}28m"
HIDE_ON="${HIDE}"
HIDE_OFF="${SHOW}"
SHOW_ON="${SHOW}"
SHOW_OFF="${HIDE}"
