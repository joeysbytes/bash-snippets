# shellcheck shell=bash

source ../colors.sh

###########################################################################
# Main Processing Logic
###########################################################################

function main() {
    print_color_table
    pause_for_keypress
    print_color_attributes_table
    pause_for_keypress
    print_attributes_table
    pause_for_keypress
    restore_screen
}


function pause_for_keypress() {
    echo ""
    read -r -p "Press ENTER to continue..."
}


function restore_screen() {
    echo -e -n "${RESET}${CLEAR_SCREEN}"
}


###########################################################################
# Color Table
###########################################################################

function print_color_table() {
    echo -e -n "${WHITE_FG}${BLACK_BG}${CLEAR_SCREEN}"
    print_color_table_heading
    print_color_row "BLACK"
    print_color_row "RED"
    print_color_row "GREEN"
    print_color_row "YELLOW"
    print_color_row "BLUE"
    print_color_row "MAGENTA"
    print_color_row "CYAN"
    print_color_row "WHITE"
    print_color_row "DEFAULT"
}


function print_color_table_heading() {
    echo "Color Table"
    echo "==========="
    echo ""
    echo "COLOR    By Name  BRIGHT_  _FG      BRI_FG   _BG      BRI_BG "
    echo "-------  -------  -------  -------  -------  -------  -------"
}


function print_color_row() {
    local row=""
    local color_name="${1}"
    local color_on
    local color_on_bg
    local color_var_name

    # Color Name Column
    row="${row}${color_name}"
    local color_name_length="${#color_name}"
    local color_name_pad_length=$((7-color_name_length))
    for ((i=0; i<color_name_pad_length; i++))
    do
        row="${row} "
    done

    # Column 1-2 separator
    row="${row}  "

    # Color Column
    color_var_name="${color_name}"
    color_on="${!color_var_name}"
    if [ "${color_name}" == "BLACK" ]; then color_on_bg="${WHITE_BG}"; else color_on_bg="${BLACK_BG}"; fi
    row="${row} ${NORMAL}${color_on_bg}${color_on}XXXXX${WHITE_FG}${BLACK_BG}   "

    # BIGHT_Color Column
    if [ "${color_name}" == "DEFAULT" ]
    then
        row="${row} n/a     "
    else
        color_var_name="BRIGHT_${color_name}"
        color_on="${!color_var_name}"
        row="${row} ${BLACK_BG}${color_on}XXXXX${WHITE_FG}${BLACK_BG}   "
    fi

    # Color_FG Column
    color_var_name="${color_name}_FG"
    color_on="${!color_var_name}"
    if [ "${color_name}" == "BLACK" ]; then color_on_bg="${WHITE_BG}"; else color_on_bg="${BLACK_BG}"; fi
    row="${row} ${color_on_bg}${color_on}XXXXX${WHITE_FG}${BLACK_BG}   "

    # BRIGHT_Color_FG Column
    if [ "${color_name}" == "DEFAULT" ]
    then
        row="${row} n/a     "
    else
        color_var_name="BRIGHT_${color_name}_FG"
        color_on="${!color_var_name}"
        row="${row} ${BLACK_BG}${color_on}XXXXX${WHITE_FG}${BLACK_BG}   "
    fi

    # Color_BG Column
    color_var_name="${color_name}_BG"
    color_on_bg="${!color_var_name}"
    if [ "${color_name}" == "DEFAULT" ]
    then
        color_on="${DEFAULT_FG}"
    elif [ "${color_name}" == "BLACK" ]
    then
        color_on="${WHITE_FG}"
    else
        color_on="${BLACK_FG}"
    fi
    row="${row} ${color_on_bg}${color_on}XXXXX${WHITE_FG}${BLACK_BG}   "

    # BRIGHT_Color_BG Column
    if [ "${color_name}" == "DEFAULT" ]
    then
        row="${row} n/a   "
    else
        color_var_name="BRIGHT_${color_name}_BG"
        color_on="${!color_var_name}"
        row="${row} ${color_on}${BLACK_FG}XXXXX${WHITE_FG}${BLACK_BG}  "
    fi

    # Output the row
    echo -e "${row}"
}


###########################################################################
# Color Attributes Table
###########################################################################

function print_color_attributes_table() {
    echo -e -n "${WHITE_FG}${BLACK_BG}${CLEAR_SCREEN}"
    print_color_attributes_table_heading
    print_color_attribute_row "BLACK"
    print_color_attribute_row "RED"
    print_color_attribute_row "GREEN"
    print_color_attribute_row "YELLOW"
    print_color_attribute_row "BLUE"
    print_color_attribute_row "MAGENTA"
    print_color_attribute_row "CYAN"
    print_color_attribute_row "WHITE"
    print_color_attribute_row "BRIGHT_BLACK"
    print_color_attribute_row "BRIGHT_RED"
    print_color_attribute_row "BRIGHT_GREEN"
    print_color_attribute_row "BRIGHT_YELLOW"
    print_color_attribute_row "BRIGHT_BLUE"
    print_color_attribute_row "BRIGHT_MAGENTA"
    print_color_attribute_row "BRIGHT_CYAN"
    print_color_attribute_row "BRIGHT_WHITE"
    print_color_attribute_row "DEFAULT"
}


function print_color_attributes_table_heading() {
    echo "Color Attributes Table"
    echo "======================"
    echo ""
    echo "COLOR           Normal     Dim     Bold    Reverse  Reve_ON"
    echo "--------------  -------  -------  -------  -------  -------"
}


function print_color_attribute_row() {
    local row=""
    local color_name="${1}"

    # Color Name Column
    row="${row}${color_name}"
    local color_name_length="${#color_name}"
    local color_name_pad_length=$((14-color_name_length))
    for ((i=0; i<color_name_pad_length; i++))
    do
        row="${row} "
    done

    # Column 1-2 separator
    row="${row}  "

    # Normal Column
    row="${row} ${NORMAL}${!color_name}XXXXX${WHITE_FG}${BLACK_BG}   "

    # Dim Column
    row="${row} ${DIM}${!color_name}XXXXX${NORMAL}${WHITE_FG}${BLACK_BG}   "

    # Bold Column
    row="${row} ${BOLD}${!color_name}XXXXX${NORMAL}${WHITE_FG}${BLACK_BG}   "

    # Reverse Column
    row="${row} ${REVERSE}${!color_name}XXXXX${REVERSE_OFF}${WHITE_FG}${BLACK_BG}   "

    # Reverse_ON Column
    row="${row} ${REVERSE_ON}${!color_name}XXXXX${REVERSE_OFF}${WHITE_FG}${BLACK_BG} "

    # Output the row
    echo -e "${row}"
}


###########################################################################
# Attributes Table
###########################################################################

function print_attributes_table() {
    echo -e -n "${WHITE_FG}${BLACK_BG}${CLEAR_SCREEN}"
    print_attributes_table_heading
    print_attribute_row "UNDERLINE"
    print_attribute_row "ITALIC"
    print_attribute_row "STRIKETHROUGH"
    print_attribute_row "BLINK"
    print_attribute_row "HIDE"
    print_attribute_row "SHOW"
}


function print_attributes_table_heading() {
    echo "Attributes Table"
    echo "================"
    echo ""
    echo "Attribute      Normal   _OFF      _ON   "
    echo "-------------  -------  -------  -------"
}


function print_attribute_row() {
    local row=""
    local attr_name="${1}"
    local attr_var_name
    local attr_var_name_off

    # Attribute Name Column
    row="${row}${attr_name}"
    local attr_name_length="${#attr_name}"
    local attr_name_pad_length=$((13-attr_name_length))
    for ((i=0; i<attr_name_pad_length; i++))
    do
        row="${row} "
    done

    # Column 1-2 separator
    row="${row}  "

    # Attribute Column
    attr_var_name="${attr_name}"
    attr_var_name_off="${attr_name}_OFF"
    row="${row} ${!attr_var_name}XXXXX${!attr_var_name_off}   "

    # Attribute_OFF Column
    row="${row} XXXXX   "

    # Attribute_ON Column
    attr_var_name="${attr_name}_ON"
    attr_var_name_off="${attr_name}_OFF"
    row="${row} ${!attr_var_name}XXXXX"
    if [ ! "${attr_name}" == "SHOW" ]; then row="${row}${!attr_var_name_off}"; fi
    row="${row} "

    # Output the row
    echo -e "${row}"
}


###########################################################################
# Begin
###########################################################################

main
