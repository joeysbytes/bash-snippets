# colors.sh

This snippet adds ANSI Color and Attribute constants.

## Color Table

| Foreground | Background | Bright Foreground | Bright Background |
| --- | --- | --- | --- |
| DEFAULT<br />DEFAULT_FG | DEFAULT_BG | n/a | n/a |
| BLACK<br />BLACK_FG | BLACK_BG | BRIGHT_BLACK<br />BRIGHT_BLACK_FG | BRIGHT_BLACK_BG |
| RED<br />RED_FG | RED_BG | BRIGHT_RED<br />BRIGHT_RED_FG | BRIGHT_RED_BG |
| GREEN<br />GREEN_FG | GREEN_BG | BRIGHT_GREEN<br />BRIGHT_GREEN_FG | BRIGHT_GREEN_BG |
| YELLOW<br />YELLOW_FG | YELLOW_BG | BRIGHT_YELLOW<br />BRIGHT_YELLOW_FG | BRIGHT_YELLOW_BG |
| BLUE<br />BLUE_FG | BLUE_BG | BRIGHT_BLUE<br />BRIGHT_BLUE_FG | BRIGHT_BLUE_BG |
| MAGENTA<br />MAGENTA_FG | MAGENTA_BG | BRIGHT_MAGENTA<br />BRIGHT_MAGENTA_FG | BRIGHT_MAGENTA_BG |
| CYAN<br />CYAN_FG | CYAN_BG | BRIGHT_CYAN<br />BRIGHT_CYAN_FG | BRIGHT_CYAN_BG |
| WHITE<br />WHITE_FG | WHITE_BG | BRIGHT_WHITE<br />BRIGHT_WHITE_FG | BRIGHT_WHITE_BG |

## Attributes Table

| Attribute On | Attribute Off |
| --- | --- |
| NORMAL | n/a |
| BOLD | NORMAL |
| DIM | NORMAL |
| BLINK<br />BLINK_ON | BLINK_OFF |
| ITALIC<br />ITALIC_ON | ITALIC_OFF |
| REVERSE<br />REVERSE_ON | REVERSE_OFF |
| STRIKETHROUGH<br />STRIKETHROUGH_ON | STRIKETHROUGH_OFF |
| UNDERLINE<br />UNDERLINE_ON | UNDERLINE_OFF |
| HIDE<br />HIDE_ON<br />SHOW_OFF | HIDE_OFF<br />SHOW<br />SHOW_ON |
| SHOW<br />SHOW_ON<br />HIDE_OFF | SHOW_OFF<br />HIDE<br />HIDE_ON |

## Misc ANSI Codes

| Constant | Description |
| --- | --- |
| CLEAR_SCREEN | Clears the screen and moves the cursor to top left corner. |
| DEFAULT_COLORS | Sets both foreground and background to default colors. |
| RESET | Sets all attributes back to defaults. |
