#!/usr/bin/env python3

import subprocess
import unittest

VARIABLES_DICT = {}
DEFAULT_VARIABLE_VALUES = { "_ESC": "\033",
                            "_CSI": "\033[",
                            "RESET": "\033[0m",
                            "CLEAR_SCREEN": "\033[2J\033[1;1H",
                            "DEFAULT_COLORS": "\033[39m\033[49m",

                            "DEFAULT": "\033[39m",
                            "BLACK": "\033[30m",
                            "RED": "\033[31m",
                            "GREEN": "\033[32m",
                            "YELLOW": "\033[33m",
                            "BLUE": "\033[34m",
                            "MAGENTA": "\033[35m",
                            "CYAN": "\033[36m",
                            "WHITE": "\033[37m",
                            "BRIGHT_BLACK": "\033[90m",
                            "BRIGHT_RED": "\033[91m",
                            "BRIGHT_GREEN": "\033[92m",
                            "BRIGHT_YELLOW": "\033[93m",
                            "BRIGHT_BLUE": "\033[94m",
                            "BRIGHT_MAGENTA": "\033[95m",
                            "BRIGHT_CYAN": "\033[96m",
                            "BRIGHT_WHITE": "\033[97m",

                            "DEFAULT_FG": "\033[39m",
                            "BLACK_FG": "\033[30m",
                            "RED_FG": "\033[31m",
                            "GREEN_FG": "\033[32m",
                            "YELLOW_FG": "\033[33m",
                            "BLUE_FG": "\033[34m",
                            "MAGENTA_FG": "\033[35m",
                            "CYAN_FG": "\033[36m",
                            "WHITE_FG": "\033[37m",
                            "BRIGHT_BLACK_FG": "\033[90m",
                            "BRIGHT_RED_FG": "\033[91m",
                            "BRIGHT_GREEN_FG": "\033[92m",
                            "BRIGHT_YELLOW_FG": "\033[93m",
                            "BRIGHT_BLUE_FG": "\033[94m",
                            "BRIGHT_MAGENTA_FG": "\033[95m",
                            "BRIGHT_CYAN_FG": "\033[96m",
                            "BRIGHT_WHITE_FG": "\033[97m",

                            "DEFAULT_BG": "\033[49m",
                            "BLACK_BG": "\033[40m",
                            "RED_BG": "\033[41m",
                            "GREEN_BG": "\033[42m",
                            "YELLOW_BG": "\033[43m",
                            "BLUE_BG": "\033[44m",
                            "MAGENTA_BG": "\033[45m",
                            "CYAN_BG": "\033[46m",
                            "WHITE_BG": "\033[47m",
                            "BRIGHT_BLACK_BG": "\033[100m",
                            "BRIGHT_RED_BG": "\033[101m",
                            "BRIGHT_GREEN_BG": "\033[102m",
                            "BRIGHT_YELLOW_BG": "\033[103m",
                            "BRIGHT_BLUE_BG": "\033[104m",
                            "BRIGHT_MAGENTA_BG": "\033[105m",
                            "BRIGHT_CYAN_BG": "\033[106m",
                            "BRIGHT_WHITE_BG": "\033[107m",

                            "NORMAL": "\033[22m",
                            "BOLD": "\033[22m\033[1m",
                            "DIM": "\033[22m\033[2m",
                            "ITALIC": "\033[3m",
                            "ITALIC_ON": "\033[3m",
                            "ITALIC_OFF": "\033[23m",
                            "UNDERLINE": "\033[4m",
                            "UNDERLINE_ON": "\033[4m",
                            "UNDERLINE_OFF": "\033[24m",
                            "BLINK": "\033[5m",
                            "BLINK_ON": "\033[5m",
                            "BLINK_OFF": "\033[25m",
                            "REVERSE": "\033[7m",
                            "REVERSE_ON": "\033[7m",
                            "REVERSE_OFF": "\033[27m",
                            "STRIKETHROUGH": "\033[9m",
                            "STRIKETHROUGH_ON": "\033[9m",
                            "STRIKETHROUGH_OFF": "\033[29m",
                            "HIDE": "\033[8m",
                            "HIDE_ON": "\033[8m",
                            "HIDE_OFF": "\033[28m",
                            "SHOW": "\033[28m",
                            "SHOW_ON": "\033[28m",
                            "SHOW_OFF": "\033[8m"
}

def run_colorssh(command):
    # set up command
    cmd = [ "/usr/bin/env", "bash", "./colors_test_runner.sh", command ]

    # run the command
    status = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                            check=False, shell=False)
    # build results dictionary
    results = {}
    results["rc"] = status.returncode
    results["args"] = status.args
    results["stdout"] = status.stdout.decode('UTF-8')
    results["stderr"] = status.stderr.decode('UTF-8')

    # return results
    return results


###########################################################################
# Unit Test colors.sh
###########################################################################

class TestColorsSh(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Load all default variables in to the dictionary to cut down reads
        cmd = ""
        for key in DEFAULT_VARIABLE_VALUES.keys():
            cmd += 'echo -e ' + key + '~"${' + key + '}";'
        results = run_colorssh(cmd)
        result_lines = results["stdout"].split('\n')
        for result_ln in result_lines:
            if len(result_ln) > 0:
                result_ln_parts = result_ln.split('~')
                VARIABLES_DICT[result_ln_parts[0]] = result_ln_parts[1]


    #----------------------------------------------------------------------
    # Testing utility methods
    #----------------------------------------------------------------------

    # def check_results(self,
    #                   results, expected_rc, expected_stdout, expected_stderr,
    #                   expected_rc_msg="Return code did not match expected value",
    #                   expected_stdout_msg="stdout did not match expected value",
    #                   expected_stderr_msg="stderr did not match expected value"):
    #     self.assertEqual(expected_rc, results["rc"], expected_rc_msg)
    #     if expected_stdout is not None:
    #         self.assertEqual(expected_stdout, results["stdout"], expected_stdout_msg)
    #     if expected_stderr is not None:
    #         self.assertEqual(expected_stderr, results["stderr"], expected_stderr_msg)


    #----------------------------------------------------------------------
    # Validate default values
    #----------------------------------------------------------------------

    def test_default_values(self):
        for variable, value in DEFAULT_VARIABLE_VALUES.items():
            with self.subTest(variable=variable, value=value):
                self.assertEqual(value, VARIABLES_DICT[variable], f"Default value not correct")


###########################################################################
# Begin
###########################################################################

if __name__ == "__main__":
    unittest.main()
