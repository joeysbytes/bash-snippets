# yes_no.sh

This provides the function yes_no, in which you can pass in a question, and the variable
YES_NO will be set to 'Y' or 'N'.

## Quick Start

```text
yes_no "Do you need some more tea?" y

Do you need some more tea? (Y/n): _
```

## Reference

### The Function Name

yes_no

### Parameters

1. The question to prompt (optional)
1. The default answer if the user just presses ENTER (optional): Y, y, N, or n
   * If a value other than these is submitted, it will be set to _no default answer_.

### The Question Prompt

The prompt will be build as follows:

1. The question prompt text
1. A single space
1. (Y/n): , (y/N): , or (y/n): to show the user the valid options.
   * The capital letter shows the submitted answer if the user just presses ENTER.
1. A single space

### The User Response

The user's response is only evaluated at the first character.  It is case-insensitive.

Valid values are: Y, y, N, n

If the user submits an empty string (ie, just presses ENTER), and a default answer was
passed in, that default answer will be used.

### The Answer

The variable YES_NO will be set to either 'Y' or 'N'.
