# shellcheck shell=bash
# shellcheck disable=SC2034

YES_NO="N"

###########################################################################
# Prompt the user with a yes/no question.
# The answer will be stored in variable YES_NO with either 'Y' or 'N'
#
# Parameters:
#   1 - Prompt question
#   2 - Default on enter (optional, default="" [no default])
###########################################################################
function yes_no() {
    YES_NO="N"
    local yn_prompt="(y/n): "
    local yn_default_answer=""
    local yn_answer=""

    # Set the default for blank response
    if [ $# -ge 2 ]
    then
        if [[ "${2}" =~ ^[Yy] ]]
        then
            yn_default_answer="Y"
            yn_prompt="(Y/n): "
        elif [[ "${2}" =~ ^[Nn] ]]
        then
            yn_default_answer="N"
            yn_prompt="(y/N): "
        else
            yn_default_answer=""
            yn_prompt="(y/n): "
        fi
    fi

    # Set the prompt
    if [ $# -ge 1 ]
    then
        yn_prompt="${1} ${yn_prompt}"
    fi

    # Ask the user, get answer
    while [[ ! "${yn_answer}" =~ ^[NnYy] ]]
    do
        echo -n "${yn_prompt}"
        read -r yn_answer
        if [ "${yn_answer}" == "" ] && [ ! "${yn_default_answer}" == "" ]
        then
            yn_answer="${yn_default_answer}"
        fi
        if [[ ! "${yn_answer}" =~ ^[NnYy] ]]
        then
            echo "Invalid response: Please answer 'y' or 'n'"
        fi
    done

    # Set the answer
    if [[ "${yn_answer}" =~ ^[Yy] ]]
    then
        YES_NO="Y"
    else
        YES_NO="N"
    fi

    echo "You answered: ${YES_NO}"
}
